import { DropdownProps } from '../Dropdown/types';
import { ButtonSize } from '../Button/types';

import { NetworksIconKeys } from './networksIcon';

export type NetworkImageDropdownItem = {
  networkId: number;
  onClick: () => void;
  iconName: NetworksIconKeys;
  testnetIdentifier?: string;
};

export interface ImageDropdownProps
  extends Omit<DropdownProps, 'header' | 'children'> {
  size?: ButtonSize;
  selectedNetworkId: number;
  width?: number;
  height?: number;
  items?: NetworkImageDropdownItem[];
}
