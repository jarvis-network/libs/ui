import React from 'react';

import { useIsMobile, useIsTablet } from '../hooks';

export const OnDesktop: React.FC = ({ children }) =>
  !useIsMobile() && !useIsTablet() ? <>{children}</> : null;
