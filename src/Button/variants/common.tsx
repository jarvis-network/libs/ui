import { ButtonModifierProps, ButtonSize } from '../types';

import { ThemeConfig } from '../../Theme';
import { customButton, roundedButton, disabledButton } from '../../common/mixins';

enum ButtonPadding {
  xxs = '6px 10px',
  xs = '7px 12px',
  s = '8px 14px',
  m = '9px 16px',
  l = '10px 18px',
  xl = '12px 22px',
  xxl = '15px 28px',
  xxxl = '18px 34px',
}

function borderRadius(theme: ThemeConfig, size?: ButtonSize) {
  if (size === 'xl' || size === 'xxl' || size === 'xxxl') {
    return theme.borderRadius.m;
  }

  return theme.borderRadius.s;
}

export const getButtonStyles = (props: ButtonModifierProps, theme: ThemeConfig) => `
  outline: none;
  align-items: center;
  background: ${theme.background.secondary};
  border: 0;
  color: ${theme.text.primary};
  cursor: pointer;
  flex-wrap: wrap;
  font-size: ${theme.font.sizes[props.size || 'xl']};
  font-weight: normal;
  font-family: inherit;
  justify-content: center;
  margin: 0;
  padding: ${ButtonPadding[props.size || 'xl']};
  text-align: left;
  text-decoration: none;
  transition: all 150ms;
  width: max-content;
  border-radius: ${borderRadius(theme, props.size)};

  ${
    props.block
      ? `
        width: 100%;
        text-align: center;
      `
      : ''
  }

  ${
    props.buttonType === 'success'
      ? customButton({
          background: theme.common.success,
          shadow: theme.shadow.base,
          hoverBackground: theme.common.primaryDarken,
          color: theme.text.active,
          border: '',
        })
      : ''
  }

  ${
    props.buttonType === 'primary'
      ? customButton({
          background: theme.button.primary.active.background,
          shadow: 'none',
          hoverBackground: theme.button.primary.hover.background,
          color: theme.button.primary.active.text,
          border: '',
        })
      : ''
  }
  ${
    props.buttonType === 'secondary'
      ? customButton({
          background: theme.button.secondary.active.background,
          shadow: 'none',
          hoverBackground: theme.button.secondary.hover.background,
          color: theme.button.secondary.active.text,
          border: '',
        })
      : ''
  }

  ${
    props.buttonType === 'ghost'
      ? customButton({
          background: theme.background.secondary,
          shadow: 'none',
          hoverBackground: theme.background.primary,
          color: theme.text.primary,
          border: `1px solid ${theme.border.primary}`,
        })
      : ''
  }

  ${
    props.buttonType === 'danger'
      ? customButton({
          background: theme.common.danger,
          shadow: theme.shadow.base,
          hoverBackground: theme.common.danger,
          color: theme.common.white,
          border: '',
        })
      : ''
  }

  ${props.rounded ? roundedButton({ borderRadius: theme.borderRadius.l }) : ''}

  ${
    props.disabled
      ? disabledButton({
          background: theme.background.disabled,
          color: theme.text.disabled,
        })
      : ''
  }
`;
