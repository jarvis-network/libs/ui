import React from 'react';

import { styled } from '../../Theme';

import { ButtonProps, ButtonDesignProps, ButtonModifierProps } from '../types';

import { getButtonStyles } from './common';

export const StandardButtonContainer = styled.button<ButtonModifierProps>(props =>
  getButtonStyles(props, props.theme),
);

interface StandardButtonProps extends ButtonProps, ButtonDesignProps {}

const StandardButton: React.FC<StandardButtonProps> = ({
  children,
  type,
  ...props
}) => (
  <StandardButtonContainer type="button" buttonType={type} {...props}>
    {children}
  </StandardButtonContainer>
);

export default StandardButton;
