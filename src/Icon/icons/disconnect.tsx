import React from 'react';

export const disconnectIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 17 17"
  >
    <g id="Disconnect" transform="translate(-329.238 -328.248)">
      <path
        id="Path_2240"
        data-name="Path 2240"
        d="M8.5,10.637a.5.5,0,0,1-.382-.822l2.729-3.247L8.117,3.322a.5.5,0,0,1,.765-.644l3,3.568a.5.5,0,0,1,0,.644l-3,3.568A.5.5,0,0,1,8.5,10.637Z"
        transform="translate(334.238 330.182)"
        fill="currentColor"
      />
      <path
        id="Line_7"
        data-name="Line 7"
        d="M11,.5H0A.5.5,0,0,1-.5,0,.5.5,0,0,1,0-.5H11a.5.5,0,0,1,.5.5A.5.5,0,0,1,11,.5Z"
        transform="translate(334.738 336.748)"
        fill="currentColor"
      />
      <path
        id="Path_2241"
        data-name="Path 2241"
        d="M7.044,17H1.809A1.888,1.888,0,0,1,0,15.045V1.955A1.888,1.888,0,0,1,1.809,0H7.044a.5.5,0,0,1,0,1H1.809A.892.892,0,0,0,1,1.955V15.045A.892.892,0,0,0,1.809,16H7.044a.5.5,0,0,1,0,1Z"
        transform="translate(329.238 328.248)"
        fill="currentColor"
      />
    </g>
  </svg>
);
