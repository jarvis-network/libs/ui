import React from 'react';

import { AllButtonProps } from '../types';

import { Button } from '..';

interface Props {
  to?: string;
  onClick?: () => void;
}

export interface ControlProps extends AllButtonProps {
  description: string;
}

export default (props: Props) => ({
  Default: () => <Button {...props}>Link button</Button>,
  SuccessButton: () => (
    <Button type="success" {...props}>
      Success button
    </Button>
  ),
  PrimaryButton: () => (
    <Button type="primary" {...props}>
      Success button
    </Button>
  ),
  SecondaryButton: () => (
    <Button type="secondary" {...props}>
      Secondary button
    </Button>
  ),
  DangerButton: () => (
    <Button type="danger" {...props}>
      Danger button
    </Button>
  ),
  RoundedButton: () => (
    <Button rounded {...props}>
      Rounded button
    </Button>
  ),
  DisabledButton: () => (
    <Button disabled {...props}>
      Disabled button
    </Button>
  ),
  TransparentButton: () => (
    <Button type="transparent" {...props}>
      Transparent button
    </Button>
  ),
  Control: ({ children }: ControlProps) => <Button {...props}>{children}</Button>,
});
