import { CSSProperties, ImgHTMLAttributes } from 'react';

import { IconKeys } from './files';

export type Size = 'small' | 'medium' | 'big';

export interface IconProps
  extends Omit<ImgHTMLAttributes<HTMLImageElement>, 'src'> {
  icon: IconKeys;
  size?: Size;
  color?: string;
  className?: string;
  noSpaceAround?: boolean;
  style?: CSSProperties;
  onClick?: React.DOMAttributes<HTMLElement>['onClick'];
}
