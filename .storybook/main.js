module.exports = {
  staticDirs: ['../public'],
  stories: ["../src/**/stories/*.stories.@(tsx|jsx)"],
  "core": {
    "builder": "@storybook/builder-webpack5"
  },
  framework: "@storybook/react",
  features: { emotionAlias: false },
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    '@storybook/addon-actions',
    "@storybook/addon-interactions",
    "@storybook/preset-create-react-app",
    "@storybook/addon-knobs",
  ],
};
