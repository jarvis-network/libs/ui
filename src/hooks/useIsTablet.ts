import { useTheme } from '../Theme';

import { useWindowSize } from './useWindowSize';

export const useIsTablet = (): boolean => {
  const theme = useTheme();
  const windowsWidth = useWindowSize().innerWidth;
  return (
    windowsWidth <= theme.rwd.breakpoints[theme.rwd.desktopIndex] &&
    windowsWidth > theme.rwd.breakpoints[theme.rwd.mobileIndex]
  );
};
