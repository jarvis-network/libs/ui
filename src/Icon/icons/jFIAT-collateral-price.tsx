import React from 'react';

export const jFIATCollateralPriceIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 24.5 24.5"
  >
    <g
      id="jFIAT_Collateral_Price"
      data-name="jFIAT/Collateral
      Price"
      transform="translate(-1.25 -1.25)"
    >
      <path
        id="Path_5088"
        data-name="Path 5088"
        d="M2,11.95a.75.75,0,0,1-.53-1.28L9.52,2.62a.75.75,0,0,1,.977-.072l6.426,4.771L24.54,1.408a.75.75,0,0,1,.92,1.185L17.393,8.853a.75.75,0,0,1-.907.01L10.123,4.138,2.53,11.73A.748.748,0,0,1,2,11.95Z"
        fill="currentColor"
      />
      <path
        id="Line_22"
        data-name="Line 22"
        d="M0,4.2a.75.75,0,0,1-.75-.75V0A.75.75,0,0,1,0-.75.75.75,0,0,1,.75,0V3.45A.75.75,0,0,1,0,4.2Z"
        transform="translate(3.15 21.55)"
        fill="currentColor"
      />
      <path
        id="Line_23"
        data-name="Line 23"
        d="M0,12.25a.75.75,0,0,1-.75-.75V0A.75.75,0,0,1,0-.75.75.75,0,0,1,.75,0V11.5A.75.75,0,0,1,0,12.25Z"
        transform="translate(10.05 13.5)"
        fill="currentColor"
      />
      <path
        id="Line_24"
        data-name="Line 24"
        d="M0,7.937a.75.75,0,0,1-.75-.75V0A.75.75,0,0,1,0-.75.75.75,0,0,1,.75,0V7.187A.75.75,0,0,1,0,7.937Z"
        transform="translate(16.95 17.812)"
        fill="currentColor"
      />
      <path
        id="Line_25"
        data-name="Line 25"
        d="M0,15.6a.75.75,0,0,1-.75-.75V0A.75.75,0,0,1,0-.75.75.75,0,0,1,.75,0V14.854A.75.75,0,0,1,0,15.6Z"
        transform="translate(23.85 10.146)"
        fill="currentColor"
      />
    </g>
  </svg>
);
