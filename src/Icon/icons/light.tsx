import React from 'react';

export const lightIcon = () => (
  <svg
    id="sun"
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 19.372 19.372"
  >
    <path
      id="Path_29"
      data-name="Path 29"
      d="M6.307,3.229a.807.807,0,0,0,.807-.807V.807A.807.807,0,1,0,5.5.807V2.421A.807.807,0,0,0,6.307,3.229Z"
      transform="translate(3.379 0)"
      fill="currentColor"
    />
    <path
      id="Path_30"
      data-name="Path 30"
      d="M9.489,4.367a.807.807,0,0,0,.571-.237L11.2,2.988A.807.807,0,1,0,10.06,1.847L8.919,2.988a.807.807,0,0,0,.57,1.379Z"
      transform="translate(5.334 0.989)"
      fill="currentColor"
    />
    <path
      id="Path_31"
      data-name="Path 31"
      d="M12.421,5.5H10.807a.807.807,0,0,0,0,1.614h1.614a.807.807,0,0,0,0-1.614Z"
      transform="translate(6.143 3.379)"
      fill="currentColor"
    />
    <path
      id="Path_32"
      data-name="Path 32"
      d="M10.06,8.919A.807.807,0,1,0,8.919,10.06L10.06,11.2A.807.807,0,1,0,11.2,10.06Z"
      transform="translate(5.334 5.334)"
      fill="currentColor"
    />
    <path
      id="Path_33"
      data-name="Path 33"
      d="M6.307,10a.807.807,0,0,0-.807.807v1.614a.807.807,0,0,0,1.614,0V10.807A.807.807,0,0,0,6.307,10Z"
      transform="translate(3.379 6.143)"
      fill="currentColor"
    />
    <path
      id="Path_34"
      data-name="Path 34"
      d="M2.988,8.919,1.847,10.06A.807.807,0,1,0,2.988,11.2L4.13,10.06A.807.807,0,1,0,2.988,8.919Z"
      transform="translate(0.989 5.334)"
      fill="currentColor"
    />
    <path
      id="Path_35"
      data-name="Path 35"
      d="M3.229,6.307A.807.807,0,0,0,2.421,5.5H.807a.807.807,0,0,0,0,1.614H2.421A.807.807,0,0,0,3.229,6.307Z"
      transform="translate(0 3.379)"
      fill="currentColor"
    />
    <path
      id="Path_36"
      data-name="Path 36"
      d="M2.988,4.13A.807.807,0,1,0,4.13,2.988L2.988,1.847A.807.807,0,1,0,1.847,2.988Z"
      transform="translate(0.989 0.989)"
      fill="currentColor"
    />
    <circle
      id="Ellipse_1"
      data-name="Ellipse 1"
      cx="5"
      cy="5"
      r="5"
      transform="translate(5 5)"
      fill="currentColor"
    />
  </svg>
);
