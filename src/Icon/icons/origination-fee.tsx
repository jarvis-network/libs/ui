import React from 'react';

export const originationFeeIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 25.5 25.5"
  >
    <g
      id="Origination_Fee"
      data-name="Origination Fee"
      transform="translate(-0.25 -0.25)"
    >
      <path
        id="Ellipse_268"
        data-name="Ellipse 268"
        d="M12-.75a12.75,12.75,0,0,1,9.016,21.766A12.75,12.75,0,1,1,2.984,2.984,12.667,12.667,0,0,1,12-.75Zm0,24a11.25,11.25,0,0,0,7.955-19.2,11.25,11.25,0,1,0-15.91,15.91A11.176,11.176,0,0,0,12,23.25Z"
        transform="translate(1 1)"
        fill="currentColor"
      />
      <path
        id="Line_17"
        data-name="Line 17"
        d="M0,16.75A.75.75,0,0,1-.75,16V0A.75.75,0,0,1,0-.75.75.75,0,0,1,.75,0V16A.75.75,0,0,1,0,16.75Z"
        transform="translate(13 5)"
        fill="currentColor"
      />
      <path
        id="Path_5072"
        data-name="Path 5072"
        d="M14.68,20.9a14.258,14.258,0,0,1-4.4-.781.75.75,0,1,1,.484-1.42c2.124.724,5.007,1.076,6.318.137a1.675,1.675,0,0,0,.69-1.488c0-1-.942-1.408-2.941-2.036a11.1,11.1,0,0,1-2.8-1.159,3.168,3.168,0,0,1-1.536-2.758,2.729,2.729,0,0,1,1.374-2.44,6.9,6.9,0,0,1,3.923-.666,9.766,9.766,0,0,1,3.458.847.75.75,0,1,1-.692,1.331,8.369,8.369,0,0,0-2.881-.682,5.44,5.44,0,0,0-3.03.452,1.25,1.25,0,0,0-.652,1.158c0,1.358,1.24,1.845,3.281,2.486a11.07,11.07,0,0,1,2.571,1.041,2.768,2.768,0,0,1,1.421,2.425,3.179,3.179,0,0,1-1.317,2.708A5.639,5.639,0,0,1,14.68,20.9Z"
        transform="translate(-1.903 -1.601)"
        fill="currentColor"
      />
    </g>
  </svg>
);
