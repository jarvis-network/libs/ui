export interface SampleData {
  id?: string;
  col1: string;
  col2: string;
  col3: string;
}
const data = [
  {
    col1: 'Hello',
    col2: 'World',
    col3: '0.9',
  },
  {
    col1: 'react-table',
    col2: 'rocks',
    col3: '0.9',
  },
  {
    col1: 'whatever',
    col2: 'you want',
    col3: '-1',
  },
  {
    col1: 'react-table',
    col2: 'rocks',
    col3: '0.9',
  },
  {
    col1: 'whatever',
    col2: 'you want',
    col3: '-1',
  },
  {
    col1: 'react-table',
    col2: 'rocks',
    col3: '0.9',
  },
  {
    col1: 'whatever',
    col2: 'you want',
    col3: '-1',
  },
  {
    col1: 'react-table',
    col2: 'rocks',
    col3: '0.9',
  },
  {
    col1: 'whatever',
    col2: 'you want',
    col3: '-1',
  },
  {
    col1: 'react-table',
    col2: 'rocks',
    col3: '0.9',
  },
  {
    col1: 'whatever',
    col2: 'you want',
    col3: '-1',
  },
  {
    col1: 'react-table',
    col2: 'rocks',
    col3: '0.9',
  },
  {
    col1: 'whatever',
    col2: 'you want',
    col3: '-1',
  },
  {
    col1: 'react-table',
    col2: 'rocks',
    col3: '0.9',
  },
  {
    col1: 'whatever',
    col2: 'you want',
    col3: '-1',
  },
  {
    col1: 'react-table',
    col2: 'rocks',
    col3: '0.9',
  },
  {
    col1: 'whatever',
    col2: 'you want',
    col3: '-1',
  },
];

const loansMockedData = [
  {
    collateral: 'USDC',
    debt: 'jEUR',
    debtValue: 50,
    collateralValue: 100,
    liquidationRatio: 105,
    currentRatio: 176.94,
  },
  {
    collateral: 'jCHF',
    debt: 'jBGN',
    debtValue: 50,
    collateralValue: 100,
    liquidationRatio: 105,
    currentRatio: 176.94,
  },
];

export { data, loansMockedData };
