import React from 'react';

export const backArrowIcon = () => (
  <svg
    id="Back_Arrow"
    data-name="Back Arrow"
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 18.641 13.013"
  >
    <path
      id="Line_1"
      data-name="Line 1"
      d="M16.641.5H0A.5.5,0,0,1-.5,0,.5.5,0,0,1,0-.5H16.641a.5.5,0,0,1,.5.5A.5.5,0,0,1,16.641.5Z"
      transform="translate(1.5 6.33)"
      fill="currentColor"
    />
    <path
      id="Path_31"
      data-name="Path 31"
      d="M6.524,14.513a.5.5,0,0,1-.353-.146L.147,8.361a.5.5,0,0,1,0-.708L6.171,1.646a.5.5,0,0,1,.706.708L1.208,8.006l5.669,5.652a.5.5,0,0,1-.353.854Z"
      transform="translate(0 -1.5)"
      fill="currentColor"
    />
  </svg>
);
