import React from 'react';

export const closeCircleIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 25 25"
  >
    <g id="Close_Loan" data-name="Close Loan" transform="translate(-0.5 -0.5)">
      <path
        id="Line_14"
        data-name="Line 14"
        d="M0,10.1a.5.5,0,0,1-.354-.146.5.5,0,0,1,0-.707l9.6-9.6a.5.5,0,0,1,.707,0,.5.5,0,0,1,0,.707l-9.6,9.6A.5.5,0,0,1,0,10.1Z"
        transform="translate(8.2 8.2)"
        fill="currentColor"
      />
      <path
        id="Line_15"
        data-name="Line 15"
        d="M9.6,10.1a.5.5,0,0,1-.354-.146l-9.6-9.6a.5.5,0,0,1,0-.707.5.5,0,0,1,.707,0l9.6,9.6A.5.5,0,0,1,9.6,10.1Z"
        transform="translate(8.2 8.2)"
        fill="currentColor"
      />
      <path
        id="Ellipse_261"
        data-name="Ellipse 261"
        d="M12-.5a12.5,12.5,0,0,1,8.839,21.339A12.5,12.5,0,0,1,3.161,3.161,12.418,12.418,0,0,1,12-.5Zm0,24A11.5,11.5,0,0,0,20.132,3.868,11.5,11.5,0,0,0,3.868,20.132,11.425,11.425,0,0,0,12,23.5Z"
        transform="translate(1 1)"
        fill="currentColor"
      />
    </g>
  </svg>
);
