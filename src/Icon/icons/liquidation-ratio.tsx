import React from 'react';

export const liquidationRatioIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 22.393 24.5"
  >
    <g
      id="Liquidation_Ratio"
      data-name="Liquidation Ratio"
      transform="translate(-1.252 -0.235)"
    >
      <path
        id="Line_19"
        data-name="Line 19"
        d="M0,3.817a.75.75,0,0,1-.75-.75V0A.75.75,0,0,1,0-.75.75.75,0,0,1,.75,0V3.067A.75.75,0,0,1,0,3.817Z"
        transform="translate(10.433 20.918)"
        fill="currentColor"
      />
      <path
        id="Line_20"
        data-name="Line 20"
        d="M0,3.817a.75.75,0,0,1-.75-.75V0A.75.75,0,0,1,0-.75.75.75,0,0,1,.75,0V3.067A.75.75,0,0,1,0,3.817Z"
        transform="translate(14.266 20.918)"
        fill="currentColor"
      />
      <path
        id="Ellipse_272"
        data-name="Ellipse 272"
        d="M2.683-.75A3.433,3.433,0,1,1-.75,2.683,3.437,3.437,0,0,1,2.683-.75Zm0,5.367A1.933,1.933,0,1,0,.75,2.683,1.936,1.936,0,0,0,2.683,4.617Z"
        transform="translate(5.066 9.418)"
        fill="currentColor"
      />
      <path
        id="Ellipse_273"
        data-name="Ellipse 273"
        d="M2.683-.75A3.433,3.433,0,1,1-.75,2.683,3.437,3.437,0,0,1,2.683-.75Zm0,5.367A1.933,1.933,0,1,0,.75,2.683,1.936,1.936,0,0,0,2.683,4.617Z"
        transform="translate(14.266 9.418)"
        fill="currentColor"
      />
      <path
        id="Path_5085"
        data-name="Path 5085"
        d="M12.444.235a11.2,11.2,0,0,1,7.022,19.924V22.94a1.8,1.8,0,0,1-1.795,1.795H7.224A1.8,1.8,0,0,1,5.429,22.94V20.158A11.2,11.2,0,0,1,12.444.235Zm5.227,23a.3.3,0,0,0,.295-.295V19.791a.75.75,0,0,1,.3-.6,9.7,9.7,0,1,0-11.638,0,.75.75,0,0,1,.3.6V22.94a.3.3,0,0,0,.295.295Z"
        transform="translate(0 0)"
        fill="currentColor"
      />
    </g>
  </svg>
);
