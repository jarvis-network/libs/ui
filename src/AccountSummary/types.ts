import { MenuDropdownLink } from '../MenuDropdown';
import { ThemeNameType } from '../Theme';
import { NetworkImageDropdownItem } from '../NetworkDropdown';

export interface AccountSummaryProps {
  // auth
  wallet?: string;
  name?: string;
  image?: string;
  isUnsupportedNetworkId?: boolean;

  // actions
  accountMenu?: MenuDropdownLink[];
  settingMenu?: MenuDropdownLink[];
  networkList?: NetworkImageDropdownItem[];
  selectedNetworkId?: number;

  // handlers
  onAccount: () => void;
  onLogin?: () => void;
  onLogout?: () => void;
  onThemeChange?: (theme: ThemeNameType) => void;

  // customization
  className?: string;
  contentOnTop?: boolean;
  authLabel?: string;
}
