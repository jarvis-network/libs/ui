import { ThemeNameType } from '../types';

export const themesList: ThemeNameType[] = ['light', 'dark', 'dusk'];

export const colorList = ['', 'red', 'green', 'blue', 'yellow', 'cyan', 'magenta'];
