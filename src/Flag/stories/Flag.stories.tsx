import React from 'react';

import { Flag } from '..';
import { Size } from '../types';
import { files } from '../files';

const flags = Object.keys(files);
const sizes = ['small', 'medium', 'big'];

export default {
  title: 'Flag',
  component: Flag,
  args: {
    size: sizes[1],
  },
};

export const AllFlagList = () => {
  const render = [];

  for (const s of sizes) {
    render.push(
      flags.map((f: any) => <Flag key={`${f}${s}`} flag={f} size={s as Size} />),
    );
    render.push(<div key={`${s}-divider`}>&nbsp;</div>);
  }

  return <div>{render}</div>;
};

export const CustomizeFlag = (args: any) => (
  <Flag flag={args!.flag} size={args!.size} />
);

CustomizeFlag.args = {
  flag: 'jBGN',
  size: sizes[1],
};

CustomizeFlag.argTypes = {
  flag: {
    options: flags,
    control: { type: 'select' }, // Automatically inferred when 'options' is defined
  },
  size: {
    options: sizes,
    control: { type: 'inline-radio' }, // Automatically inferred when 'options' is defined
  },
};
