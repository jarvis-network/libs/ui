import React, { useState } from 'react';

import { styled } from '../Theme';

import { flexRow, flexColumn } from '../common/mixins';

import { InputProps } from './types';

const INPUT_HEIGHT = 60;
const INPUT_PADDING = 16;

const InputContainer = styled.div`
  height: 100%;
  width: 100%;
`;

const InputGroupContainer = styled.div<{
  filled: boolean;
  isInvalid?: boolean;
  isSearch?: boolean;
  isFocus?: boolean;
}>`
  ${flexRow()}
  align-items: center;
  height: 100%;
  width: 100%;
  border: 1px solid
    ${props =>
      props.isSearch
        ? props.theme.border.secondary
        : props.isFocus
        ? props.theme.border.active
        : props.theme.border.primary};
  border-radius: ${props =>
    props.isSearch ? props.theme.borderRadius.xl : props.theme.borderRadius.s};
  input {
    border: 0;
    color: ${props => props.theme.text.primary};
    background: ${props => props.theme.background.primary};
    font-size: ${props => props.theme.font.sizes.l};
    height: ${INPUT_HEIGHT}px;
    outline: 0;
    height: 100%;
    width: 100%;
  }

  ${props =>
    props.isInvalid
      ? `
    border: 1px solid ${props.theme.border.invalid};
  `
      : ''}
`;

const InputWrapper = styled.div<{ isSearch?: boolean; hasPrefix?: boolean }>`
  overflow: hidden;
  position: relative;
  height: 100%;
  width: 100%;
  padding: 0
    ${props => (props.isSearch && !props.hasPrefix ? '25' : INPUT_PADDING)}px;
  border-radius: ${props => props.theme.borderRadius.xl};
  input::placeholder {
    color: ${props => props.theme.text.secondary};
  }
`;

const InputPrefix = styled.div<{ isSearch?: boolean }>`
  ${flexColumn()}
  align-items: center;
  height: auto;
  padding-left: ${props => (props.isSearch ? '25' : INPUT_PADDING)}px;
`;

const InputSuffix = styled(InputPrefix)<{ isSearch?: boolean }>`
  padding-left: 0;
  padding-right: ${props => (props.isSearch ? '25' : INPUT_PADDING)}px;
`;

const InputInfo = styled.div<{ isInvalid?: boolean }>`
  font-size: ${props => props.theme.font.sizes.s};
  color: ${props =>
    props.isInvalid ? props.theme.text.invalid : props.theme.text.primary};
  margin-top: 8px;
  margin-left: ${INPUT_PADDING}px;
`;

export const Input: React.FC<InputProps> = ({
  className,
  info,
  isInvalid,
  invalidMessage,
  prefix,
  suffix,
  value,
  isSearch,
  ...props
}) => {
  const [isFocus, setIsFocus] = useState(false);
  const message = isInvalid ? invalidMessage : info;
  return (
    <InputContainer className={className || ''}>
      <InputGroupContainer
        filled={value !== ''}
        isInvalid={isInvalid}
        isSearch={isSearch}
        isFocus={isFocus}
        className="group"
      >
        {!!prefix && <InputPrefix isSearch={isSearch}>{prefix}</InputPrefix>}
        <InputWrapper isSearch={isSearch} hasPrefix={!!prefix}>
          <input
            value={value}
            {...props}
            onFocus={() => setIsFocus(true)}
            onBlur={() => setIsFocus(false)}
          />
        </InputWrapper>
        {!!suffix && <InputSuffix isSearch={isSearch}>{suffix}</InputSuffix>}
      </InputGroupContainer>
      {message && <InputInfo isInvalid={isInvalid}>{message}</InputInfo>}
    </InputContainer>
  );
};
