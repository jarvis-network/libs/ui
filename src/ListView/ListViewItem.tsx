import React from 'react';

import { styled } from '../Theme';

export interface LabelProps {
  className?: string;
}

const ListViewItemContainer = styled.div`
  font-size: ${props => props.theme.font.sizes.l};
`;

export const ListViewItem: React.FC<LabelProps> = ({
  children,
  className,
  ...props
}) => (
  <ListViewItemContainer className={className || ''} {...props}>
    {children}
  </ListViewItemContainer>
);
