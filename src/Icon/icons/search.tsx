import React from 'react';

export const searchIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 15.002 15.002"
  >
    <path
      id="Search"
      d="M18618.146,18703.855l-3.418-3.422a6.506,6.506,0,1,1,.705-.705l3.422,3.418a.509.509,0,0,1,0,.709.521.521,0,0,1-.357.145A.51.51,0,0,1,18618.146,18703.855ZM18605,18695.5a5.5,5.5,0,1,0,5.5-5.5A5.507,5.507,0,0,0,18605,18695.5Z"
      transform="translate(-18603.998 -18688.998)"
      fill="currentColor"
    />
  </svg>
);
