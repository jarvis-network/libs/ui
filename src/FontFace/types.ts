import { ReactNode } from 'react';

import { InjectKrubFontFace } from './Krub';
import { InjectKaiseiFontFace } from './KaiseiHarunoUmi';

export enum FontFace {
  KRUB = '"Krub", sans-serif',
  KAISEI = '"KAISEI"',
}

export const FontFaceMap = {
  [FontFace.KRUB]: InjectKrubFontFace,
  [FontFace.KAISEI]: InjectKaiseiFontFace,
} as const;

export interface BodyFontFaceProviderProps {
  font: FontFace;
  children: ReactNode;
}
