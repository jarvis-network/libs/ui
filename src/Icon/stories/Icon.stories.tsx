import React from 'react';

import { styled } from '../../Theme';

import { Icon } from '..';
import { Size } from '../types';
import { iconNames } from '../files';

const sizes = ['small', 'medium', 'big'];

export default {
  title: 'Icon',
  component: Icon,
  args: {
    size: sizes[1],
  },
};

const IconWrapper = styled.span`
  padding: 5px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const IconsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const IconSizeWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 5px;
  border-bottom: 1px solid #000;
  padding-bottom: 20px;
`;

export const AllImagesList = () => {
  const render = [];

  for (const s of sizes) {
    render.push(
      <div key={`${s}-divider-size`}>
        <strong>{s.toUpperCase()}</strong>
      </div>,
    );
    render.push(<div key={`${s}-divider-up`}>&nbsp;</div>);
    render.push(
      <IconSizeWrapper>
        {iconNames.map((f: any) => (
          <IconWrapper>
            <Icon key={`${f}-${s}`} icon={f} size={s as Size} />
            <span>{f}</span>
          </IconWrapper>
        ))}
      </IconSizeWrapper>,
    );
    render.push(<div key={`${s}-divider-down`}>&nbsp;</div>);
  }

  return <IconsWrapper>{render}</IconsWrapper>;
};

export const CustomizeIcon = (args: any) => (
  <Icon icon={args!.icon} color={args!.color} size={args!.size} />
);

CustomizeIcon.args = {
  icon: 'collateral',
  size: sizes[1],
  color: '#0f0',
  spaceAround: true,
};

CustomizeIcon.argTypes = {
  icon: {
    options: iconNames,
    control: { type: 'select' }, // Automatically inferred when 'options' is defined
  },
  size: {
    options: sizes,
    control: { type: 'inline-radio' }, // Automatically inferred when 'options' is defined
  },
  color: {
    control: {
      type: 'color',
      presetsColors: ['#000', '#ff0000', '#00ff00', '#0000ff', '#fff'],
    },
  },
  noSpaceAround: {
    control: 'boolean',
  },
  className: {
    control: false,
  },
  style: {
    control: false,
  },
  onClick: {
    control: false,
  },
};
