import React from 'react';

export const darkIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 14.528 14.578"
  >
    <path
      id="Path_2364"
      data-name="Path 2364"
      d="M17.217,11.673a6.575,6.575,0,0,1-1.83.251,6.886,6.886,0,0,1-6.87-6.9A6.7,6.7,0,0,1,8.767,3.2a.254.254,0,0,0-.075-.251.241.241,0,0,0-.251-.075A7.418,7.418,0,0,0,10.4,17.44,7.359,7.359,0,0,0,17.518,12a.254.254,0,0,0-.075-.251C17.418,11.673,17.318,11.648,17.217,11.673Z"
      transform="translate(-3 -2.862)"
      fill="currentColor"
    />
  </svg>
);
