import React from 'react';

import { ThemedStory } from '../src/common/ThemedStory';
import { MemoryRouter } from 'react-router-dom';

const themeDecoratorIgnore = [
  'common/Theme',
  'common/FontFace',
  'Background',
  'accountdropdown--interactive',
];

export const parameters = {
  controls: { expanded: true, hideNoControlsWarning: true },
};

export const decorators = [
  (Story, info) => {
    if (
      themeDecoratorIgnore.includes(info.kind) ||
      themeDecoratorIgnore.includes(info.id)
    ) {
      return <Story/>;
    }
    return (
      <ThemedStory>
        <MemoryRouter>
          <Story />
        </MemoryRouter>
      </ThemedStory>
    );
  },
];
