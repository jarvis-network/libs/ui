import React, { FC, ReactNode } from 'react';

import { styled } from '../Theme';

import { Emoji } from '../Emoji';
import { EmojiKeys } from '../Emoji/types';

import { IconButton } from '../IconButton';

import { CardTabs, CardTabsProps } from './CardTabs';

const Wrapper = styled.div`
  width: 100%;
  background: ${props => props.theme.background.primary};
  border-radius: 0 0 ${props => props.theme.borderRadius.m}
    ${props => props.theme.borderRadius.m};
`;

export interface CardProps extends Omit<CardTabsProps, 'tabs'> {
  title: string;
  children: ReactNode;
  titleBackground?: boolean;
  leftEmoji?: EmojiKeys;
  onBack?: () => void;
}

const BackIcon = styled(IconButton)`
  height: ${props => props.theme.sizes.row};
  border-radius: ${props => props.theme.borderRadius.m};
  background: ${props => props.theme.background.secondary};
`;
const CloseIcon = styled(IconButton)`
  height: ${props => props.theme.sizes.row};
  padding: 0px;
  margin-right: -12px;
`;

const MoneyEmoji = styled(Emoji)`
  height: ${props => props.theme.sizes.row};
  padding: 0px 20px;
  font-size: ${props => props.theme.font.sizes.xl};
`;

export const Card: FC<CardProps> = ({ title, children, onBack, ...props }) => (
  <CardTabs
    {...props}
    pre={
      onBack ? (
        <BackIcon
          onClick={onBack}
          icon="backArrowIcon"
          type="transparent"
          size="xxl"
        />
      ) : null
    }
    tabs={[
      {
        title,
        content: <Wrapper>{children}</Wrapper>,
      },
    ]}
  />
);

const CardFooterWrapper = styled.div`
  width: 93%;
  position: absolute;
  right: 0;
  left: 0;
  margin: 0 auto;
  border-bottom-left-radius: ${props => props.theme.borderRadius.m};
  border-bottom-right-radius: ${props => props.theme.borderRadius.m};

  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.mobileIndex - 1]}px) {
    width: 88%;
  }
`;

export const CardFooter = ({ children }: any) => (
  <CardFooterWrapper>{children}</CardFooterWrapper>
);

export const CardAssets: FC<CardProps> = ({
  title,
  children,
  onBack,
  leftEmoji = 'MoneyBag',
  ...props
}) => (
  <CardTabs
    {...props}
    pre={<MoneyEmoji emoji={leftEmoji!} />}
    extra={
      onBack ? (
        <CloseIcon
          onClick={onBack}
          icon="closeIcon"
          type="transparent"
          size="xxl"
        />
      ) : null
    }
    tabs={[
      {
        title,
        content: <Wrapper>{children}</Wrapper>,
      },
    ]}
  />
);
