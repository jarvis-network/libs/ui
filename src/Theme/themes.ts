import { FontFace } from '../FontFace';

import { ThemeConfig, ThemeName } from './types';

export const lightTheme: ThemeConfig = {
  name: 'light',
  sizes: {
    row: '60px',
  },
  rwd: {
    breakpoints: [1024, 767, 480],
    desktopIndex: 0,
    tabletIndex: 1,
    mobileIndex: 2,
  },
  common: {
    primary: '#4EFA74',
    primaryDarken: '#48EF6D',
    secondary: '#404040',
    success: '#4EFA74',
    danger: '#FA4E4E',
    warning: '#FFD834',
    disabled: '#4E4E4E',
    white: '#FFFFFF',
    black: '#242424',
  },
  text: {
    primary: '#242424',
    secondary: '#BEBEBE',
    medium: '#7e7e7e',
    invalid: '#FA4E4E',
    active: '#242424',
    disabled: '#BEBEBE',
    inverted: '#FFFFFF',
  },
  background: {
    primary: '#F8F8F8',
    secondary: '#FFFFFF',
    medium: '#FAFAFA',
    inverted: '#000',
    disabled: '#F1F1F1',
    active: '#242424',
    hover: '#363636',
  },
  button: {
    primary: {
      active: {
        background: '#4EFA74',
        text: '#242424',
      },
      hover: {
        background: '#48EF6D',
        text: '#242424',
      },
      disabled: {
        background: '#F1F1F1',
        text: '#BEBEBE',
      },
    },
    secondary: {
      active: {
        background: '#242424',
        text: '#FFFFFF',
      },
      hover: {
        background: '#363636',
        text: '#FFFFFF',
      },
      disabled: {
        background: '#F1F1F1',
        text: '#BEBEBE',
      },
    },
    ghost: {
      active: {
        background: '#FFFFFF',
        text: '#242424',
      },
      hover: {
        background: '#F8F8F8',
        text: '#242424',
      },
      disabled: {
        background: '#F8F8F8',
        text: '#BEBEBE',
      },
    },
  },
  border: {
    primary: '#F1F1F1',
    secondary: '#BEBEBE',
    active: '#242424',
    invalid: '#FA4E4E',
  },
  gray: {
    gray100: '#fefefe',
    gray200: '#f1f1f1',
    gray300: '#f7f7f7',
    gray400: '#7e7e7e',
  },
  font: {
    family: FontFace.KRUB,
    sizes: {
      xxs: '10px',
      xs: '12px',
      s: '14px',
      m: '16px', // default
      l: '18px',
      xl: '25px',
      xxl: '30px',
      xxxl: '60px',
    },
  },
  shadow: {
    base: '0px 0px 20px #00000029',
    small: '0px 0px 12px #00000000',
    hover: '0px 0px 15px #0000001A',
  },
  option: {
    background: '#4d4d4d',
  },
  tooltip: {
    info: {
      background: '#C9F1FF',
      questionMark: '#4FC8F4',
      questionMarkHover: '#4C90E2',
      text: '#4C90E2',
    },
    success: {
      background: '#242424',
      text: '#FFFFFF',
    },
    error: {
      background: '#FFE4E4',
      text: '#242424',
    },
  },
  scroll: {
    background: '#FFFFFF',
    thumb: '#4E4E4E',
  },
  borderRadius: {
    xxs: '3px',
    xs: '6px',
    s: '10px',
    m: '15px',
    l: '20px',
    xl: '25px',
    xxl: '30px',
  },
};

export const darkTheme: ThemeConfig = {
  ...lightTheme,
  name: 'dark',
  text: {
    primary: '#FFFFFF',
    secondary: '#BEBEBE',
    medium: '#7E7E7E',
    invalid: '#FA4E4E',
    active: '#242424',
    inverted: '#242424',
    disabled: '#7E7E7E',
  },
  background: {
    primary: '#292929',
    secondary: '#242424',
    medium: '#252525',
    inverted: '#DEDEDE',
    disabled: '#4E4E4E',
    active: '#FFFFFF',
    hover: '#4E4E4E',
  },
  button: {
    primary: {
      active: {
        background: '#4EFA74',
        text: '#242424',
      },
      hover: {
        background: '#48EF6D',
        text: '#242424',
      },
      disabled: {
        background: '#4E4E4E',
        text: '#BEBEBE',
      },
    },
    secondary: {
      active: {
        background: '#7E7E7E',
        text: '#FFFFFF',
      },
      hover: {
        background: '#4E4E4E',
        text: '#FFFFFF',
      },
      disabled: {
        background: '#4E4E4E',
        text: '#7E7E7E',
      },
    },
    ghost: {
      active: {
        background: '#292929',
        text: '#FFFFFF',
      },
      hover: {
        background: '#242424',
        text: '#FFFFFF',
      },
      disabled: {
        background: '#292929',
        text: '#7E7E7E',
      },
    },
  },
  border: {
    primary: '#4E4E4E',
    secondary: '#bebebe',
    active: '#F1F1F1',
    invalid: '#FA4E4E',
  },
  shadow: {
    base: '0px 0px 20px #00000029',
    small: '0px 0px 8px #00000000',
    hover: '0px 0px 15px #00000066',
  },
  scroll: {
    background: '#292929',
    thumb: 'rgba(0,0,0,0.3)',
  },
};

export const duskTheme: ThemeConfig = {
  ...lightTheme,
  name: 'dusk',
  text: {
    primary: '#FFFFFF',
    secondary: '#BEBEBE',
    medium: '#63758d',
    invalid: '#FA4E4E',
    active: '#242424',
    inverted: '#FFFFFF',
    disabled: '#63758D',
  },
  background: {
    primary: '#2E3541',
    secondary: '#212A34',
    medium: '#212A34',
    inverted: '#e4dcd3',
    disabled: '#262d38',
    active: '#63758D',
    hover: '#48576C',
  },
  button: {
    primary: {
      active: {
        background: '#4EFA74',
        text: '#242424',
      },
      hover: {
        background: '#48EF6D',
        text: '#242424',
      },
      disabled: {
        background: '#212A34',
        text: '#BEBEBE',
      },
    },
    secondary: {
      active: {
        background: '#63758D',
        text: '#FFFFFF',
      },
      hover: {
        background: '#48576C',
        text: '#FFFFFF',
      },
      disabled: {
        background: '#2E3541',
        text: '#63758D',
      },
    },
    ghost: {
      active: {
        background: '#2E3541',
        text: '#FFFFFF',
      },
      hover: {
        background: '#48576C',
        text: '#FFFFFF',
      },
      disabled: {
        background: '#2E3541',
        text: '#63758D',
      },
    },
  },
  border: {
    primary: '#63758D',
    secondary: '#bebebe',
    active: '#FFFFFF',
    invalid: '#FA4E4E',
  },
  shadow: {
    base: '0px 0px 20px #00000029',
    small: '0px 0px 12px #00000000',
    hover: '0px 0px 15px #00000080',
  },
  scroll: {
    background: '#2e3541',
    thumb: 'rgba(0,0,0,0.3)',
  },
};

export const themesMap = {
  [ThemeName.light]: lightTheme,
  [ThemeName.dark]: darkTheme,
  [ThemeName.dusk]: duskTheme,
} as const;
