import { ReactNode } from 'react';

export interface InputProps {
  className?: string;
  info?: string;
  isInvalid?: boolean;
  isSearch?: boolean;
  invalidMessage?: string;
  prefix?: ReactNode;
  suffix?: ReactNode;
  value?: string;
  [key: string]: any;
}
