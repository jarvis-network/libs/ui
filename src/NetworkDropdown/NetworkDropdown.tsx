import React from 'react';

import { styled } from '../Theme';

import { Dropdown } from '../Dropdown';
import { ImageButton } from '../ImageButton';

import { ImageDropdownProps } from './types';
import { networksIcon } from './networksIcon';

const CustomDropdown = styled(Dropdown)`
  background: transparent;
  box-shadow: none;
`;

const Content = styled.div<{ contentOnTop?: boolean }>`
  border: 1px solid ${props => props.theme.border.primary};
  border-radius: ${props => props.theme.borderRadius.s};
  background: ${props => props.theme.background.secondary};

  ${props =>
    props.contentOnTop
      ? `
    margin-bottom: 10px;
  `
      : `
    margin-top: 10px;
  `}
`;

const CustomImageButton = styled(ImageButton)`
  border-radius: ${props => props.theme.borderRadius.s};
  background: ${props => props.theme.background.secondary};
`;

export const NetworkDropdown: React.FC<ImageDropdownProps> = ({
  items,
  selectedNetworkId = 0,
  size = 'm',
  width,
  height,
  contentOnTop = false,
  ...props
}) => {
  if (!items?.length) {
    return null;
  }
  const activeItem = items?.findIndex(val => val.networkId === selectedNetworkId);

  return (
    <CustomDropdown
      {...props}
      contentOnTop={contentOnTop}
      header={
        <CustomImageButton
          size={size}
          src={
            networksIcon[items[activeItem].iconName as keyof typeof networksIcon]
          }
          alt={items[activeItem].iconName}
          testnetIdentifier={items[activeItem].testnetIdentifier}
          height={height}
          width={width}
        />
      }
      width="36px"
    >
      {items.length > 1 && (
        <Content contentOnTop={contentOnTop}>
          {items
            ?.filter(({ networkId }) => networkId !== items[activeItem].networkId)
            .map(({ iconName, onClick, networkId, testnetIdentifier }) => (
              <CustomImageButton
                key={networkId}
                alt={iconName}
                size={size}
                src={networksIcon[iconName as keyof typeof networksIcon]}
                testnetIdentifier={testnetIdentifier}
                height={height}
                width={width}
                onClick={onClick}
              />
            ))}
        </Content>
      )}
    </CustomDropdown>
  );
};
