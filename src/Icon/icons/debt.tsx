import React from 'react';

export const debtIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 25.5 23.616"
  >
    <g id="Debt" transform="translate(314.75 -1239.5) rotate(90)">
      <g id="hands-heart" transform="translate(1238 295.236)">
        <path
          id="Path_5045"
          data-name="Path 5045"
          d="M25,29.514a.75.75,0,0,1-.75-.75V26.977a.75.75,0,0,1,.181-.488l4.716-5.5a1.932,1.932,0,0,0,.465-1.257V11.787a1.037,1.037,0,1,0-2.074,0v4.84a.75.75,0,0,1-1.5,0v-4.84a2.537,2.537,0,1,1,5.074,0V19.73a3.433,3.433,0,0,1-.826,2.233L25.75,27.254v1.51A.75.75,0,0,1,25,29.514Z"
          transform="translate(-5.995 -10)"
          fill="currentColor"
        />
        <path
          id="Path_5046"
          data-name="Path 5046"
          d="M18,30.246a.75.75,0,0,1-.75-.75V23.459a3.408,3.408,0,0,1,1.005-2.427L22.384,16.9a2.645,2.645,0,0,1,3.741,3.741L23,23.772a.75.75,0,0,1-1.061-1.061l3.127-3.127a1.145,1.145,0,1,0-1.62-1.62l-4.129,4.129a1.918,1.918,0,0,0-.566,1.366V29.5A.75.75,0,0,1,18,30.246Z"
          transform="translate(-5.25 -10.732)"
          fill="currentColor"
        />
      </g>
      <path
        id="Ellipse_255"
        data-name="Ellipse 255"
        d="M6-.75A6.75,6.75,0,1,1-.75,6,6.758,6.758,0,0,1,6-.75Zm0,12A5.25,5.25,0,1,0,.75,6,5.256,5.256,0,0,0,6,11.25Z"
        transform="translate(1240.25 290)"
        fill="currentColor"
      />
    </g>
  </svg>
);
