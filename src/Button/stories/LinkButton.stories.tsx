import React from 'react';

import { ComponentStory } from '@storybook/react';

import { Button } from '..';

import { buttonTypes } from '../types';

import stories from './ButtonStories';

export default {
  title: 'Button/LinkButton',
  component: Button,
  argTypes: {
    to: {
      control: false,
    },
    className: {
      control: false,
    },
    onClick: {
      control: false,
    },
    inverted: {
      control: false,
    },
    block: {
      control: false,
    },
  },
};

const {
  Default,
  SuccessButton,
  PrimaryButton,
  SecondaryButton,
  DangerButton,
  DisabledButton,
  TransparentButton,
  RoundedButton,
} = stories({
  to: '/route',
});

const Control: ComponentStory<typeof Button> = ({ text, ...props }) => (
  <Button {...props}>{text}</Button>
);
const CustomControlButton = Control.bind({});
CustomControlButton.args = {
  type: 'success',
  rounded: false,
  disabled: false,
  size: 'm',
  text: 'Button with link',
  to: '/route',
};
CustomControlButton.argTypes = {
  type: {
    options: buttonTypes,
    control: {
      type: 'select',
    },
  },
  rounded: { control: 'boolean' },
  disabled: { control: 'boolean' },
  text: { control: 'text' },
  size: { control: 'select', options: ['xxs', 'xs', 's', 'm', 'l', 'xl', 'xxl'] },
};

export {
  Default,
  SuccessButton,
  PrimaryButton,
  SecondaryButton,
  DangerButton,
  DisabledButton,
  TransparentButton,
  RoundedButton,
  CustomControlButton,
};
