import React from 'react';

export const liquidationPenaltyIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 22.626 25.5"
  >
    <g
      id="Liquidation_Penalty"
      data-name="Liquidation Penalty"
      transform="translate(-2.9 -0.25)"
    >
      <path
        id="Path_5073"
        data-name="Path 5073"
        d="M23.772,26.75h-6.84a.75.75,0,0,1-.359-.092c-.136-.074-3.322-1.846-3.322-4.658V16a3,3,0,0,1,3.193-2.75A3,3,0,0,1,19.636,16v4.4a.75.75,0,1,1-1.5,0V16c0-.678-.775-1.25-1.693-1.25S14.75,15.322,14.75,16v6c0,1.64,1.836,2.907,2.388,3.25h6.111l2.458-6.7a1.9,1.9,0,0,0,.048-1.178L23.4,9.042a2.525,2.525,0,0,0-1.278-1.489l-1.64-.9A.75.75,0,1,1,21.2,5.342l1.64.9a4.007,4.007,0,0,1,2,2.4L27.2,16.97a3.394,3.394,0,0,1-.083,2.1l-2.638,7.187A.75.75,0,0,1,23.772,26.75Z"
        transform="translate(-1.8 -1)"
        fill="currentColor"
      />
      <path
        id="Path_5074"
        data-name="Path 5074"
        d="M10.76,19.35H5a.75.75,0,0,1-.75-.75V1A.75.75,0,0,1,5,.25H19.15A.75.75,0,0,1,19.9,1V11.4a.75.75,0,0,1-1.5,0V1.75H5.75v16.1h5.01a.75.75,0,0,1,0,1.5Z"
        transform="translate(-1.35)"
        fill="currentColor"
      />
    </g>
  </svg>
);
