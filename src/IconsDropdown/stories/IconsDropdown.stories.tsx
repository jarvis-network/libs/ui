import React, { useState } from 'react';

import { IconsDropdown } from '..';

export default {
  title: 'Dropdown/IconsDropdown',
  component: IconsDropdown,
};

const noop = () => undefined;

export const Default = () => (
  <IconsDropdown
    items={[
      {
        icon: 'dropDownArrowIcon',
        onClick: noop,
      },
      {
        icon: 'backArrowIcon',
        onClick: noop,
      },
      {
        icon: 'removeCollateralIcon',
        onClick: noop,
      },
    ]}
  />
);

export const Stateful = () => {
  const [active, setActive] = useState(0);

  const onChange = (index: number) => () => setActive(index);

  return (
    <IconsDropdown
      active={active}
      items={[
        {
          icon: 'dropDownArrowIcon',
          onClick: onChange(0),
        },
        {
          icon: 'backArrowIcon',
          onClick: onChange(1),
        },
        {
          icon: 'removeCollateralIcon',
          onClick: onChange(2),
        },
      ]}
    />
  );
};
