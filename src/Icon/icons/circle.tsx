import React from 'react';

export const circleIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    fill="#ff0000"
    viewBox="0 0 16 16"
  >
    <g id="Info" transform="translate(-0.5 0.5)">
      <circle
        id="Ellipse_13"
        data-name="Ellipse 13"
        cx="7.5"
        cy="7.5"
        r="7.5"
        transform="translate(1)"
        fill="none"
        stroke="currentColor"
        strokeLinecap="square"
        strokeMiterlimit="10"
        strokeWidth="1"
      />
    </g>
  </svg>
);
