import React from 'react';

import { useIsTablet } from '../hooks';

export const OnTablet: React.FC = ({ children }) =>
  useIsTablet() ? <>{children}</> : null;
