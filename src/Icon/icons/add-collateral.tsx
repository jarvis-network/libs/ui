import React from 'react';

export const addCollateralIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 25 25"
  >
    <g
      id="Add_Collateral"
      data-name="Add Collateral"
      transform="translate(0.5 -4.5)"
    >
      <path
        id="Line_12"
        data-name="Line 12"
        d="M0,20.214a.5.5,0,0,1-.5-.5V0A.5.5,0,0,1,0-.5.5.5,0,0,1,.5,0V19.714A.5.5,0,0,1,0,20.214Z"
        transform="translate(12 5)"
        fill="currentColor"
      />
      <path
        id="Path_5066"
        data-name="Path 5066"
        d="M14.714,10.214a.5.5,0,0,1-.354-.146L6.646,2.354a.5.5,0,1,1,.707-.707l7.361,7.361,7.361-7.361a.5.5,0,0,1,.707.707l-7.714,7.714A.5.5,0,0,1,14.714,10.214Z"
        transform="translate(-2.714 15)"
        fill="currentColor"
      />
      <path
        id="Line_13"
        data-name="Line 13"
        d="M24,.5H0A.5.5,0,0,1-.5,0,.5.5,0,0,1,0-.5H24a.5.5,0,0,1,.5.5A.5.5,0,0,1,24,.5Z"
        transform="translate(0 29)"
        fill="currentColor"
      />
    </g>
  </svg>
);
