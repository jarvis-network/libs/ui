import React from 'react';

export const liquidationPriceIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 24.5 24.385"
  >
    <g
      id="Liquidation_Price"
      data-name="Liquidation Price"
      transform="translate(0.75 0.75)"
    >
      <path
        id="Path_5088"
        data-name="Path 5088"
        d="M25,14.4a.748.748,0,0,1-.55-.24l-7.575-8.18-6.426,4.636a.75.75,0,0,1-.969-.078L1.47,2.53A.75.75,0,0,1,2.53,1.47l7.558,7.558,6.447-4.651a.75.75,0,0,1,.989.1l8.026,8.667A.75.75,0,0,1,25,14.4Z"
        transform="translate(-2 -2)"
        fill="currentColor"
      />
      <path
        id="Line_22"
        data-name="Line 22"
        d="M0,4.183a.75.75,0,0,1-.75-.75V0A.75.75,0,0,1,0-.75.75.75,0,0,1,.75,0V3.433A.75.75,0,0,1,0,4.183Z"
        transform="translate(21.74 19.452)"
        fill="currentColor"
      />
      <path
        id="Line_23"
        data-name="Line 23"
        d="M0,10.762a.75.75,0,0,1-.75-.75V0A.75.75,0,0,1,0-.75.75.75,0,0,1,.75,0V10.012A.75.75,0,0,1,0,10.762Z"
        transform="translate(14.875 12.873)"
        fill="currentColor"
      />
      <path
        id="Line_24"
        data-name="Line 24"
        d="M0,9.808a.75.75,0,0,1-.75-.75V0A.75.75,0,0,1,0-.75.75.75,0,0,1,.75,0V9.058A.75.75,0,0,1,0,9.808Z"
        transform="translate(8.01 13.826)"
        fill="currentColor"
      />
      <path
        id="Line_25"
        data-name="Line 25"
        d="M0,14.576a.75.75,0,0,1-.75-.75V0A.75.75,0,0,1,0-.75.75.75,0,0,1,.75,0V13.826A.75.75,0,0,1,0,14.576Z"
        transform="translate(1.144 9.059)"
        fill="currentColor"
      />
    </g>
  </svg>
);
