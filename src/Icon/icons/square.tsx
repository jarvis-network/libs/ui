import React from 'react';

export const squareIcon = () => (
  <svg
    fill="none"
    height="100%"
    viewBox="0 0 24 24"
    width="100%"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect height="85%" rx="3" stroke="black" width="85%" x="3" y="3" />
  </svg>
);
