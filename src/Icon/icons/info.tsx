import React from 'react';

export const infoIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    fill="#ff0000"
    viewBox="0 0 16 16"
  >
    <g id="Info" transform="translate(-0.5 0.5)">
      <circle
        id="Ellipse_13"
        data-name="Ellipse 13"
        cx="7.5"
        cy="7.5"
        r="7.5"
        transform="translate(1)"
        fill="none"
        stroke="currentColor"
        strokeLinecap="square"
        strokeMiterlimit="10"
        strokeWidth="1"
      />
      <circle
        id="Ellipse_15"
        data-name="Ellipse 15"
        cx="0.5"
        cy="0.5"
        r="0.5"
        transform="translate(8 11)"
        fill="none"
        stroke="currentColor"
        strokeLinecap="square"
        strokeMiterlimit="10"
        strokeWidth="1"
      />
      <path
        id="Path_2262"
        data-name="Path 2262"
        d="M19,11.4c1.417-.627,3.252-.552,3.879.462s.194,2.194-.88,3.1S20.7,16.42,20.7,17.135"
        transform="translate(-12.205 -7.93)"
        fill="none"
        stroke="currentColor"
        strokeLinecap="square"
        strokeMiterlimit="10"
        strokeWidth="1"
      />
    </g>
  </svg>
);
