import BTC from './icons/BTC.svg';
import bUSD from './icons/BUSD.svg';
import jAUD from './icons/jAUD.svg';
import jBGN from './icons/jBGN.svg';
import jBRL from './icons/jBRL.svg';
import jCAD from './icons/jCAD.svg';
import jCHF from './icons/jCHF.svg';
import jCOP from './icons/jCOP.svg';
import jEUR from './icons/jEUR.svg';
import jGBP from './icons/jGBP.svg';
import jJPY from './icons/jJPY.svg';
import jCNY from './icons/jCNY.svg';
import jMXN from './icons/jMXN.svg';
import jNGN from './icons/jNGN.svg';
import jPHP from './icons/jPHP.svg';
import jSEK from './icons/jSEK.svg';
import jSGD from './icons/jSGD.svg';
import jTRY from './icons/jTRY.svg';
import jXAF from './icons/jXAF.png';
import jXOF from './icons/jXOF.png';
import USDC from './icons/USDC.svg';
import wXDAI from './icons/wXDAI.svg';

export const files = {
  BTC,
  bUSD,
  jAUD,
  jBGN,
  jBRL,
  jCAD,
  jCHF,
  jCOP,
  jEUR,
  jGBP,
  jJPY,
  jCNY,
  jMXN,
  jNGN,
  jPHP,
  jSEK,
  jSGD,
  jTRY,
  jXAF,
  jXOF,
  USDC,
  wXDAI,
};

export type FlagKeys = keyof typeof files;

export { default as questionMark } from './icons/question-mark.svg';
