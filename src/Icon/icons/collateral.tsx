import React from 'react';

export const collateralIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 24 24"
  >
    <defs>
      <clipPath id="clip-path">
        <rect
          id="Rectangle_1052"
          data-name="Rectangle 1052"
          width="100%"
          height="100%"
          transform="translate(631 224)"
          fill="currentColor"
        />
      </clipPath>
    </defs>
    <g id="Collateral" transform="translate(-631 -224)" clipPath="url(#clip-path)">
      <g
        id="Group_4489"
        data-name="Group 4489"
        transform="translate(632.043 221.913)"
      >
        <path
          id="Subtraction_2"
          data-name="Subtraction 2"
          d="M-3078.65,16974.879a10.409,10.409,0,0,1-10.395-10.4,10.407,10.407,0,0,1,10.395-10.395,10.386,10.386,0,0,1,1.718.143l-1.368,1.367c-.1,0-.217-.006-.349-.006a8.9,8.9,0,0,0-8.9,8.891,8.907,8.907,0,0,0,8.9,8.9,8.908,8.908,0,0,0,8.9-8.9c0-.187-.006-.377-.018-.564l1.345-1.344a10.458,10.458,0,0,1,.176,1.908A10.411,10.411,0,0,1-3078.65,16974.879Z"
          transform="translate(3088.293 -16949.086)"
          fill="currentColor"
        />
        <g id="hyperlink" transform="translate(5.588 13.719) rotate(-45)">
          <path
            id="Path_5101"
            data-name="Path 5101"
            d="M0,8.7a.75.75,0,0,1-.75-.75A.75.75,0,0,1,0,7.2H2.384A3.19,3.19,0,0,0,5.608,3.974,3.186,3.186,0,0,0,2.384.75H0A.75.75,0,0,1-.75,0,.75.75,0,0,1,0-.75H2.384A4.685,4.685,0,0,1,7.108,3.974,4.674,4.674,0,0,1,2.384,8.7Z"
            transform="translate(11.127 0)"
            fill="currentColor"
          />
          <path
            id="Path_5102"
            data-name="Path 5102"
            d="M6.358,8.7H3.974A4.685,4.685,0,0,1-.75,3.974,4.674,4.674,0,0,1,3.974-.75H6.358a.75.75,0,0,1,.75.75.75.75,0,0,1-.75.75H3.974A3.19,3.19,0,0,0,.75,3.974,3.186,3.186,0,0,0,3.974,7.2H6.358a.75.75,0,1,1,0,1.5Z"
            transform="translate(0 0)"
            fill="currentColor"
          />
          <path
            id="Line_28"
            data-name="Line 28"
            d="M0,.75A.75.75,0,0,1-.75,0,.75.75,0,0,1,0-.75H6.358a.75.75,0,0,1,.75.75.75.75,0,0,1-.75.75Z"
            transform="translate(5.564 3.974)"
            fill="currentColor"
          />
        </g>
      </g>
    </g>
  </svg>
);
