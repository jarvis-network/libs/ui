import React from 'react';

export const copyIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 17.003 17.003"
  >
    <g id="Copy" transform="translate(-295 -385.907)">
      <path
        id="Path_2345"
        data-name="Path 2345"
        d="M310.227,403.182h-7.273A1.957,1.957,0,0,1,301,401.227V399.97a.5.5,0,1,1,1,0v1.257a.956.956,0,0,0,.955.955h7.273a.956.956,0,0,0,.955-.955v-7.273a.956.956,0,0,0-.955-.955h-1.149a.5.5,0,0,1,0-1h1.149a1.957,1.957,0,0,1,1.955,1.955v7.273A1.957,1.957,0,0,1,310.227,403.182Z"
        transform="translate(-0.179 -0.272)"
        fill="currentColor"
      />
      <path
        id="Rectangle_363"
        data-name="Rectangle 363"
        d="M1-.5h9A1.5,1.5,0,0,1,11.5,1v9A1.5,1.5,0,0,1,10,11.5H1A1.5,1.5,0,0,1-.5,10V1A1.5,1.5,0,0,1,1-.5Zm9,11a.5.5,0,0,0,.5-.5V1A.5.5,0,0,0,10,.5H1A.5.5,0,0,0,.5,1v9a.5.5,0,0,0,.5.5Z"
        transform="translate(295.5 386.407)"
        fill="currentColor"
      />
    </g>
  </svg>
);
