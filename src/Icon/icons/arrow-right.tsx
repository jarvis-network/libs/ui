import React from 'react';

export const arrowRightIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 13.353 7.176"
  >
    <path
      id="Arrow_Right"
      data-name="Arrow Right"
      d="M6721.751,5744.956l5.97,5.969,5.969-5.969"
      transform="
            rotate(-90)
            translate(-6731.044 -5739.249)
        "
      fill="none"
      stroke="currentColor"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="1"
    />
  </svg>
);
