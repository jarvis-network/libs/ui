import React, { FC } from 'react';
import { Global, css } from '@emotion/react';

import Bold from './KaiseiHarunoUmi-Bold.ttf';
import Medium from './KaiseiHarunoUmi-Medium.ttf';
import Regular from './KaiseiHarunoUmi-Regular.ttf';

export const InjectKaiseiFontFace: FC = () => (
  <Global
    styles={css`
      @font-face {
        font-family: 'Kaisei';
        src: url('${Regular}') format('truetype');
      }
      @font-face {
        font-family: 'Kaisei';
        font-weight: 500;
        src: url('${Medium}') format('truetype');
      }
      @font-face {
        font-family: 'Kaisei';
        font-weight: 700;
        src: url('${Bold}') format('truetype');
      }
    `}
  />
);
