import React, { useState } from 'react';

import { Switcher } from '../Switcher';

export default {
  title: 'Switcher',
  component: Switcher,
};

const items = ['Stop', 'Slow', 'Fast', 'Instant'];

export const Default = () => {
  const [selected, setSelected] = useState(0);

  return (
    <Switcher
      items={items}
      onChange={value => setSelected(items.indexOf(value))}
      selected={selected}
    />
  );
};
