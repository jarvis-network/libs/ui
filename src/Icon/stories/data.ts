import { iconNames } from '../files';

const iconList = iconNames.slice();

const colorList = ['black', 'red', 'green', 'blue', 'yellow', 'magenta', 'cyan'];

export { iconList, colorList };
