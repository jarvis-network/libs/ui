import React from 'react';

import { action } from '@storybook/addon-actions';

import { ImageButton } from '../ImageButton';
import { networksIcon } from '../../NetworkDropdown';

export default {
  title: 'Button/ImageButton',
  component: ImageButton,
  argTypes: {
    type: {
      table: {
        disable: true,
      },
    },
    block: {
      table: {
        disable: true,
      },
    },
    inverted: {
      table: {
        disable: true,
      },
    },
    disabled: {
      table: {
        disable: true,
      },
    },
    rounded: {
      table: {
        disable: true,
      },
    },
    size: {
      table: {
        disable: true,
      },
    },
    className: {
      control: true,
    },
    src: {
      control: true,
    },
    alt: {
      control: true,
    },
    to: {
      control: true,
    },
  },
};

export const Default = () => (
  <ImageButton src={networksIcon.Eth} alt="Mainnet" onClick={action('clicked')} />
);

export const Inline = () => (
  <div>
    <div>
      Eth logo:
      <ImageButton
        onClick={action('clicked')}
        src={networksIcon.Eth}
        alt="Mainnet"
        inline
      />
    </div>
    <div>
      <ImageButton
        onClick={action('clicked')}
        src={networksIcon.BSC}
        alt="Bsc"
        inline
      />
      Binance Smart chain
    </div>
  </div>
);

export const Control = (args: any) => {
  const { image, width, height, inline, testnetIdentifier } = args;
  const images: { [key in string]: { src: any; alt: string } } = {
    Eth: {
      src: networksIcon.Eth,
      alt: 'Eth',
    },
    Bsc: {
      src: networksIcon.BSC,
      alt: 'Bsc',
    },
    Matic: {
      src: networksIcon.Matic,
      alt: 'Matic',
    },
  };

  return (
    <ImageButton
      onClick={action('clicked')}
      src={images[image].src}
      alt={images[image].alt}
      width={width}
      height={height}
      inline={inline}
      testnetIdentifier={testnetIdentifier}
    />
  );
};

Control.args = {
  inline: false,
  image: 'Eth',
  height: 24,
  width: 15,
  testnetIdentifier: '',
};

Control.argTypes = {
  inline: { control: 'boolean' },
  image: { control: 'select', options: ['Eth', 'Bsc', 'Matic'] },
  width: { control: 'number', min: 1 },
  height: { control: 'number', min: 1 },
  testnetIdentifier: { control: 'text' },
};
