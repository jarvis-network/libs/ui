import React from 'react';

import { useIsMobile } from '../hooks';

export const OnMobile: React.FC = ({ children }) =>
  useIsMobile() ? <>{children}</> : null;
