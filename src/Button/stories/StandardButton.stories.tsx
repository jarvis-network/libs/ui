import { action } from '@storybook/addon-actions';

import { ComponentStory } from '@storybook/react';

import React from 'react';

import { Button } from '..';

import { buttonTypes } from '../types';

import stories from './ButtonStories';

export default {
  title: 'Button/StandardButton',
  component: Button,
  argTypes: {
    to: {
      control: false,
    },
    className: {
      control: false,
    },
    onClick: {
      control: false,
    },
    inverted: {
      control: false,
    },
    block: {
      control: false,
    },
  },
};

const {
  Default,
  SuccessButton,
  PrimaryButton,
  SecondaryButton,
  DangerButton,
  DisabledButton,
  TransparentButton,
  RoundedButton,
} = stories({
  onClick: action('clicked'),
});

const Control: ComponentStory<typeof Button> = ({ text, ...props }) => (
  <Button {...props}>{text}</Button>
);
const CustomControlButton = Control.bind({});
CustomControlButton.args = {
  type: 'success',
  rounded: false,
  disabled: false,
  size: 'm',
  text: 'Button with link',
  onClick: action('clicked'),
};
CustomControlButton.argTypes = {
  type: {
    options: buttonTypes,
    control: {
      type: 'select',
    },
  },
  rounded: { control: 'boolean' },
  disabled: { control: 'boolean' },
  text: { control: 'text' },
  size: { control: 'select', options: ['xxs', 'xs', 's', 'm', 'l', 'xl', 'xxl'] },
};

export {
  Default,
  SuccessButton,
  PrimaryButton,
  SecondaryButton,
  DangerButton,
  DisabledButton,
  TransparentButton,
  RoundedButton,
  CustomControlButton,
};
